import React, { Component } from 'react';
import * as actions from '../../actions/LabelActions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MyLabelList from '../../components/myLabelList';
import TextField from 'material-ui/lib/text-field';
import style from './index.css';


@connect(
  (state) => ({labels: state.labels}),
  (dispatch) => bindActionCreators(actions, dispatch)
)
export default class Labels extends Component {
  
  static propTypes = {
    attachLabel: React.PropTypes.func.isRequired,
    deleteLabel: React.PropTypes.func.isRequired,
    detachLabel: React.PropTypes.func.isRequired,
    setUnChecked: React.PropTypes.func.isRequired,
    setChecked: React.PropTypes.func.isRequired,
    labels: React.PropTypes.object.isRequired,
  }
  
  state = { filter: '' }

  _onChange = (e) => {
  	this.setState({ filter:  e.target.value })
  }

	_onEnterKeyDown = (e) => {
		if(e.target.value.length === 0) {
			return
		}
		this.props.addLabel(e.target.value);
		this.setState({ filter:  '' });
	}

  render() {

    let { labels,
          attachLabel,
          detachLabel,
          setUnChecked,
          deleteLabel,
          setChecked } = this.props;

    return (
    	<div className={style.labels}>
        <h3>Label</h3>
        <TextField floatingLabelText="name of lable"
                   value={this.state.filter}
        					 onEnterKeyDown={this._onEnterKeyDown}
        					 onChange={this._onChange}
        />
        <MyLabelList labels={labels}
                     attachLabel ={attachLabel}
                     detachLabel ={detachLabel}
                     labelName={ this.state.filter }
                     deleteLabel= { deleteLabel }
                     setUnChecked= { setUnChecked }
                     setChecked= { setChecked } />
      </div>
    );
  }
}