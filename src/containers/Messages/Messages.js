import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import MyMessageList from '../../components/myMessageList';
import * as actions from '../../actions/messagesActions';
import style from './index.css';


@connect(
  (state) => ({messages: state.messages}),
  (dispatch) => bindActionCreators(actions, dispatch)
)
export default  class Messages extends Component {

  static propTypes = {
    selectMes: React.PropTypes.func.isRequired,
    resetAllLabel: React.PropTypes.func.isRequired,
    messages: React.PropTypes.array.isRequired
  }

  render() {

    const { messages, selectMes, resetAllLabel } = this.props;
    return (
    	<div className={style.messages}>
        <h3>Messages</h3>
      	<MyMessageList messages={messages}
                       selectMes={selectMes}
                       resetAllLabel={resetAllLabel}
                       />
      </div>
    );
  }
}