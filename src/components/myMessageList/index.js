import React, { Component } from 'react';
import Table from 'material-ui/lib/table/table';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableHeader from 'material-ui/lib/table/table-header';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import TableBody from 'material-ui/lib/table/table-body';
import RaisedButton from 'material-ui/lib/raised-button';
import style from './style.css';

const cellStyle = {
  padding: "0",
  whiteSpace: 'normal',
  width: "200px",
  textAlign: "center"
};

class MyMessageList extends Component {

  static propTypes = {
    selectMes: React.PropTypes.func.isRequired,
    messages: React.PropTypes.array.isRequired,
    resetAllLabel: React.PropTypes.func.isRequired
  }

  _getTableHeader = () => (
      <TableHeader enableSelectAll={false} displaySelectAll={false}>
        <TableRow>
          <TableHeaderColumn style={{width: '20px'}}>
            <RaisedButton label="Reset"
                          style={{position: 'absolute',
                                  top: '16px',
                                  left: '-68px',
                                  'minWidth': '60px'}}
                          onClick={this.props.resetAllLabel}
            />
            ID
          </TableHeaderColumn>
          <TableHeaderColumn>Name</TableHeaderColumn>
          <TableHeaderColumn style={cellStyle}>LABEL</TableHeaderColumn>
          <TableHeaderColumn>EMAIL</TableHeaderColumn>
        </TableRow>
      </TableHeader>
    )

  render() {
    
    const { messages, selectMes } = this.props;

    return (
      <Table multiSelectable={true} >
        { this._getTableHeader() }
        <TableBody deselectOnClickaway={false} >

          { messages.map((el,key) => (
            <TableRow key = { key } selected={el.checked} onRowClick={() => selectMes(el)} >
              <TableRowColumn style={{width: '20px'}}>{el.id}</TableRowColumn>
              <TableRowColumn>{el.name}</TableRowColumn>
              <TableRowColumn style={cellStyle}>
                {el.labels.map( (lab, i) =>
                  <span key = { i } className={style.label} style={{background: lab.color}}>
                    {lab.name}
                  </span>
                )}
              </TableRowColumn>
              <TableRowColumn>{el.email}</TableRowColumn>
            </TableRow>
            )
          )}

        </TableBody>
      </Table>
    )
  }
}

export default  MyMessageList;