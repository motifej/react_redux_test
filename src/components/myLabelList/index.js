import React, { Component } from 'react';
import List from 'material-ui/lib/lists/list';
import ListItem from 'material-ui/lib/lists/list-item';
import IconButton from 'material-ui/lib/icon-button';
import ActionDelete from 'material-ui/lib/svg-icons/action/delete';
import ActionGrade from 'material-ui/lib/svg-icons/action/grade';
import ActionHighlightOff from 'material-ui/lib/svg-icons/action/highlight-off';
import styles from './style.css';

class MyLabelList extends Component {

  static propTypes = {
    attachLabel: React.PropTypes.func.isRequired,
    deleteLabel: React.PropTypes.func.isRequired,
    detachLabel: React.PropTypes.func.isRequired,
    setUnChecked: React.PropTypes.func.isRequired,
    setChecked: React.PropTypes.func.isRequired,
    labels: React.PropTypes.object.isRequired,
  }

  _getButtonGroop = (el) => {
    const { labels,
            deleteLabel,
            setUnChecked,
            detachLabel } = this.props;
            
    const unStarLabel = e => {
      if (labels.selMes.length) {
        e.stopPropagation();
        detachLabel(el);
        setUnChecked(el);
      }
    }

    return (
      <div>
        <IconButton onClick={ unStarLabel }
                    style={ !el.marked ? {display: 'none'} : {}}
                    touch={true}
                    tooltipPosition="bottom-left">
          <ActionHighlightOff />
        </IconButton>
        <IconButton onClick={ e => { e.stopPropagation(); deleteLabel(el) } }
                    tooltip="remove"
                    touch={true}
                    tooltipPosition="bottom-right">
          <ActionDelete color={el.color}/>
        </IconButton>
      </div>
    )
  }

  _listItemClick = (el) => {
    let { attachLabel, setChecked, labels } = this.props;
    if(labels.selMes.length) {
      attachLabel(el);
      setChecked(el);
    }
  }

  render() {
    let { labelName, labels } = this.props;
    let filteredLable = labels.list.filter(
        el => el.name.toLocaleLowerCase().indexOf(labelName) != -1);

    return (
      <List>
        {filteredLable.map( (el,key) => 
          <ListItem key={key}
            rightIconButton={ this._getButtonGroop(el) }
            onClick={ this._listItemClick.bind(this, el) }
            primaryText={el.name}
          />
        )}
      </List>
    )
  }

}

export default  MyLabelList;