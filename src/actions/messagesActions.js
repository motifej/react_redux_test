import * as types from '../constants/ActionTypes';

export function selectMes(mes) {
  return {
    type: types.SELECT_MES,
    mes
  };
}

export function resetAllLabel() {
  return {
    type: types.RESET_ALL_LABEL
  };
}