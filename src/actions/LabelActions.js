import * as types from '../constants/ActionTypes';

export function addLabel(name) {
  return {
    type: types.ADD_LABEL,
    name
  };
}

export function deleteLabel(label) {
  return {
    type: types.DEL_LABEL,
    label
  };
}

export function attachLabel(label) {
  return {
    type: types.ATTACH_LABEL,
    label
  };
}

export function detachLabel(label) {
  return {
    type: types.DETACH_LABEL,
    label
  };
}

export function setChecked(label) {
  return {
    type: types.SET_CHECKED,
    label
  };
}

export function setUnChecked(label) {
  return {
    type: types.SET_UNCHECKED,
    label
  };
}