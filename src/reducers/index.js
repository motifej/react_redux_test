import { combineReducers } from 'redux';
import labels from './labels';
import messages from './messages';

const rootReducer = combineReducers({
  labels,
  messages
});

export default rootReducer;