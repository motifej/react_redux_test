import * as types from '../constants/ActionTypes';
import { messages } from './defaultData'

export default function (state = messages, action) {

  switch (action.type) {

  case types.ATTACH_LABEL:
    return state.map(
      el =>  el.checked && !el.labels.some(lab => lab.id == action.label.id)
                ? {...el, labels: [...el.labels, action.label]}
                : el
    );

  case types.DETACH_LABEL:
    return state.map(
      el =>  el.checked
                ? { ...el, labels: el.labels.filter( lab => lab.id !== action.label.id ) }
                : el
    );

  case types.DEL_LABEL:
    return state.map(
      el => ({...el, labels: el.labels.filter( lab => lab.id !== action.label.id ) })
    );     

  case types.SELECT_MES:
    return state.map(
      el => action.mes.id == el.id ? {...el, checked: !el.checked } : el
    );

  case types.RESET_ALL_LABEL:
    return state.map(
      el => ({...el, labels: [] }) );

  default:
    return state;
  }
}