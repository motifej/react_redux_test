import * as types from '../constants/ActionTypes';
import { uniqueId } from 'lodash';
import { labels } from './defaultData'

const labList = selMes => 
                selMes.reduce( (val, el) => [...el.labels, ...val], [] );

const filterById = ( list, el ) => 
                      list.filter( item => item.id !== el.id );

const hasItem = ( list, el ) => 
                    list.some(item => item.id == el.id);                     

const createLabel = name => ({
    name,
    id: uniqueId(),
    marked: false,
    color: `rgb(${~~(255*Math.random())},${~~(255*Math.random())},${~~(255*Math.random())})`
})



export default function (state = labels, action) {
  const { list, selMes } = state;
  
  switch (action.type) {

  case types.ADD_LABEL:
    return {
      selMes,
      list: [ ...list, createLabel(action.name) ]
    };

  case types.DEL_LABEL:
    return {
      selMes,
      list: filterById(list, action.label)
    };

  case types.RESET_ALL_LABEL:
    return {
      selMes: selMes.map(
                el => ({...el, labels: [] }) ),
      list: list.map(
              lab => ({ ...lab, marked: false }) )
    };

  case types.SET_CHECKED:
    return {
      selMes: selMes.map(
              el => !hasItem(el.labels, action.label)
                      ? {...el, labels: [...el.labels, action.label]} : el ),
      list: list.map(
              lab =>  selMes.length && lab.id === action.label.id
                        ? { ...lab, marked: true } : lab )
    };

  case types.SET_UNCHECKED:
    return {
      selMes: selMes.map(
                el => ({ ...el, labels: filterById(el.labels, action.label) }) ),
      list: list.map(
              lab => lab.id === action.label.id
                        ? { ...lab, marked: false } : lab )
    }


  case types.SELECT_MES:
    const messages = action.mes.checked
                      ? filterById(selMes, action.mes)
                      : [...selMes, action.mes];
    return {
      selMes: messages,
      list: list.map(
              lab => ({ ...lab, marked: hasItem( labList(messages), lab) }) )
    }

  default:
    return state;
  }

}