import { uniqueId } from 'lodash';

export const messages = [{
    email: 'aaa@aaa.aa',
    name: 'aaaa',
    checked: false,
    id: uniqueId(),
    labels: []
  },
  {
    email: 'bbb@bbb.bb',
    name: 'bbbb',
    checked: false,
    id: uniqueId(),
    labels: []
  },
  { email: 'ccc@ccc.cc',
    name: 'cccc',
    checked: false,
    id: uniqueId(),
    labels: []
  },
  { email: 'dddd@ddd.dd',
    name: 'dddd',
    checked: false,
    id: uniqueId(),
    labels: []
  }];

export const labels = {
    selMes: [],
    list: [{
      name: 'AAAAAA',
      marked: false,
      id: uniqueId(),
      color: 'red'
    },
    {
      name: 'BBBBBB',
      marked: false,
      id: uniqueId(),
      color: 'blue'
    },
    {
      name: 'CCCCCC',
      marked: false,
      id: uniqueId(),
      color: 'green'
    }]
};