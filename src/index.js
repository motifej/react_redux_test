import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import Messages from './containers/Messages/Messages';
import Labels from './containers/Labels/Labels';
import configureStore from './store/configureStore';
//import DevTools from './containers/dev-tools';


const store = configureStore();


render((
    <Provider store={store}>
      <div>
        <Messages />
        <Labels />
        {/*<DevTools />*/}
      </div>
    </Provider>
	), document.getElementById('root'));